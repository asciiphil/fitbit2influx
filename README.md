Work in progress.

Most resource types are idempotent, but not badges.  Badges only show up
in the API once each, with the most recent date earned.  Repeatedly
running the importer will leave old earned dates intact, but if the old
data is deleted, the importer won't be able to recreate it the way it can
everything else.
